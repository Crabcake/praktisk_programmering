#include<stdio.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<gsl/gsl_integration.h>
//#define M_PI 3.14159265358979323846
 

double f(double x, void *params) //GSL integration. This is infinite at zero so it should be solved with qags.
{
	//(void) params;
	double f = log(x)/sqrt(x);
	return f;
}

double normint(double x, void *params) //Norm-integral from problem 2
{
	double alpha = *(double*)params;
	double f = exp(-alpha*x*x);
	return f;
}

double hamint(double x, void *params)
{
	double alpha = *(double*)params;
	double f = (-alpha*alpha*x*x/2.0+alpha/2.0+x*x/2.0)*exp(-alpha*x*x);
	return f;
}

int main()
{
//Starting the first problem
{
printf("\nProblem 1\n");
gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000); //nr of steps should at least be less than this....
double result,error;	
gsl_function F;
F.function = &f;
double epsrel=1e-6,epsabs=1e-6;
gsl_integration_qags(&F,0.0,1.0,epsabs,epsrel,1000,w,&result,&error); //6 for 41 point G-K rule for smooth functions
fprintf(stdout,"Result=\t%1.4f\n",result);
fprintf(stdout,"This result matches the value calculated using wolfram alpha\n");
gsl_integration_workspace_free (w);
}
	
//Starting the second problem
{
printf("\nProblem 2\n");
double alpha_min=0.01, alpha_max=10.0,alpha_steps=10000;
double alpha_step_size = (alpha_max-alpha_min)/alpha_steps;
FILE *DATA = fopen("data.txt","w");
for(double alpha = alpha_min;alpha<(alpha_max+alpha_step_size);alpha+=alpha_step_size)
{
gsl_integration_workspace *w_norm = gsl_integration_workspace_alloc(1000); 
gsl_integration_workspace *w_ham  = gsl_integration_workspace_alloc(1000);

double norm_result,ham_result,norm_error,ham_error;	
gsl_function Hint;
gsl_function Nint;
Nint.function = &normint;
Nint.params = &alpha;
Hint.function = &hamint;
Hint.params = &alpha;
double epsrel=1e-6,epsabs=1e-6;
gsl_integration_qagi(&Nint,epsabs,epsrel,1000,w_norm,&norm_result,&norm_error); 
gsl_integration_qagi(&Hint,epsabs,epsrel,1000,w_ham ,&ham_result ,&ham_error ); 
double result = ham_result/norm_result;
fprintf(DATA,"%g\t%g\n",alpha,result);
gsl_integration_workspace_free (w_norm);
gsl_integration_workspace_free (w_ham );
}
printf("The result can be seen in plot.pdf. The minimum can be seen at alpha=1 with an energy of 0.5 matching the analytical result in unit of hbar*omega\n");
}



	
return 0;
}
