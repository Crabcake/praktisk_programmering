#include<stdio.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<gsl/gsl_matrix.h>
#define M_PI 3.14159265358979323846

//Solving the first problem

int diff_eq(double t, const double y[], double dydt[], void *params) //Simply following the gsl recipe so far. This will contain the diff eq:
{
	dydt[0]=y[0]*(1-y[0]);
	return GSL_SUCCESS;
}


int orbit_eq(double t, const double y[], double dydt[], void *params)
{
	double epsilon = *(double *) params; //See GSL ODE example on getting the values from params
	dydt[0]=y[1];
	dydt[1]=1-y[0]+epsilon*y[0]*y[0];
	return GSL_SUCCESS;
}

int main()
{

{ //Scoping each problem	
printf("\nOrbit: Problem 1\n");

//The diff eq system
gsl_odeiv2_system diff_eq_sys;
diff_eq_sys.function = diff_eq; //Choosing which diff eq system to solve
diff_eq_sys.jacobian = NULL; //No need for jacobian so it is replaced by NULL as described in defining the ODE system
diff_eq_sys.dimension = 1; //1 dim system...
diff_eq_sys.params = NULL; //No parameters needed


//Now for the driver. It needs certain values
double hstart=1e-6,epsabs=1e-6,epsrel=1e-6; //initial stepsize,epsabs and epsrel
const gsl_odeiv2_step_type *diff_eq_stepper = gsl_odeiv2_step_rkf45; //Defining step function. This is good for general ode problems.
gsl_odeiv2_driver *diff_eq_driver = gsl_odeiv2_driver_alloc_y_new(&diff_eq_sys,diff_eq_stepper,hstart,epsabs,epsrel); //See GSL ODE DRIVER

//Following GSL example applying solver
double t_start=0.0,t_end=3.0,points = 50.0,y[1]={0.5}; //Start, end, eval points, start value for y
double del_t = (t_end-t_start)/points;
FILE *diff_eq_data=fopen("diff_eq_dat.txt","w"); //For plotting later
for(double t = t_start; t<t_end+del_t;t+=del_t)
	{
	      int status = gsl_odeiv2_driver_apply (diff_eq_driver, &t_start, t, y);
	      if (status != GSL_SUCCESS)
		{
			printf ("error, return value=%d\n", status);
			break;
	  	}
	fprintf(diff_eq_data,"%g\t%g\n",t,y[0]);
	}
	printf("Final value for integration is: %.10g\n",y[0]);
	double analytic=1./(1+exp(-3.0));
	printf("The analytical result is given by f(x))=1/(1+exp(-3))=%.10g\nSee DiffEqPlot.pdf for graph\n",analytic);
	gsl_odeiv2_driver_free (diff_eq_driver);
}



{
// We will now attempt handling the tougher problem using the exact same recipe as above. Changes are e.g then need for epsilon in params and increased dimension
printf("\n\nOrbit: Problem 2\nSee OrbitPlot.pdf for results.\n");

gsl_odeiv2_system orbit_eq_sys;
orbit_eq_sys.function=orbit_eq;
orbit_eq_sys.jacobian= NULL;
orbit_eq_sys.dimension= 2;
orbit_eq_sys.params=NULL;


double epsilon[3] = {0.0,0.0,0.05}; //We need three seperate solutions
double uprime[3]  = {0.0,-0.7,-0.5};
int points = 5000;
double t_start=0.0*M_PI, t_end=10.0*M_PI;
double t[3] = {t_start,t_start,t_start}; //Need to keep t_start fixed
double del_t=(t_end-t_start)/points;
gsl_matrix *Values; //Matrix for storing numbers
Values = gsl_matrix_alloc(4,points);

for (int i=0;i<3;i++) //We need to solve three seperate instances of the problem, but we want them 
{

orbit_eq_sys.params = &epsilon[i]; //The i'th epsilon start value

double hstart=1e-6,epsabs=1e-6,epsrel=1e-6; 
const gsl_odeiv2_step_type *orbit_eq_stepper = gsl_odeiv2_step_rkf45;
gsl_odeiv2_driver *orbit_eq_driver = gsl_odeiv2_driver_alloc_y_new(&orbit_eq_sys,orbit_eq_stepper,hstart,epsabs,epsrel); 

double y[2]={1.0,uprime[i]};	//Setting the i'th value of uprime
//double t=t_start; //for looping over integers instead...
for(int j = 0; j<points ; j++)
	{
  	      double ti=t_start+del_t*(j+1);
	      int status = gsl_odeiv2_driver_apply (orbit_eq_driver, &t[i], ti, y);
	      if (status != GSL_SUCCESS)
		{
			printf ("error, return value=%d\n", status);
			break;
	  	}
	      if (i==0)
		{
		gsl_matrix_set(Values,i,j,ti);
		}
	gsl_matrix_set(Values,i+1,j,y[0]);
	//printf("%g\t%g\n",t[i],y[0]);
	}
	
	gsl_odeiv2_driver_free (orbit_eq_driver);
}
//Putting data nicely in data.txt
FILE *DATA = fopen("data.txt","w");
for (int j=0;j<points;j++)
{
fprintf(DATA,"%2.5g\t%2.5g\t%2.5g\t%2.5g\n",gsl_matrix_get(Values,0,j),gsl_matrix_get(Values,1,j),gsl_matrix_get(Values,2,j),gsl_matrix_get(Values,3,j));
}

gsl_matrix_free(Values);
}

return 0;
}
