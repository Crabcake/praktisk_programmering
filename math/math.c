# include "stdio.h"
# include "tgmath.h"
int main()
{
	printf("\nProblem 1:\n");
	double x=5;
	double gx= tgamma(x);
	printf("gamma(%g)=%g\n",x,gx);
	

	double x1=0.5;
	double Bessel=j1(x1);
	printf("The Bessel function of %g is %g\n",x1,Bessel);
	
	double	 x3=2;
	_Complex Root=cpow(-x3,0.5);
	printf("The root of minus 2 is %f + %fi\n", creal(Root), cimag(Root));
	

	_Complex ei=cpow(M_E,I);	
	printf("The exponential of i is %f + %fi\n", creal(ei), cimag(ei));

	_Complex eipi=cpow(M_E,M_PI*I);	
	printf("The exponential of i times pi is %f + %fi\n", creal(eipi), cimag(eipi));

	_Complex pie=cpow(I,M_E);	
	printf("I to the power of e is %f + %fi\n\n", creal(pie), cimag(pie));

	/* The following is part two about the number of digits stored in various data types */
	printf("Problem 2:\n");
	float x_float = 0.1111111111111111111111111111;
	double x_double = 0.1111111111111111111111111111;	
	long double x_long_double = 0.1111111111111111111111111111L;
	
	printf("The following is the number '0.1111111111111111111111111111' stored in various data types\n");

	
	printf("This is the number stored in a float:\t%.25g\nThis is the number stored in a double:\t%.25lg\nThis is the number stored in Ldouble:\t%.25Lg\n",x_float,x_double,x_long_double);

	
	return 0;
	
	
}
