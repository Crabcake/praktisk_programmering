#include<stdio.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<assert.h>


double sqrt_calculator(double t); 

int main()
{
printf("\n\nExProb5\n\nx\tcalc-sqrt(x)\tmath.h-sqrt\n");
FILE *sqrt_data = fopen("data.txt","w");

double x_start=0.0,x_end=2.0,x_points=39;
double x_delta=(x_end-x_start)/x_points;

for (double j=x_start;j<(x_end+x_delta*0.001);j+=x_delta)
{
	fprintf(sqrt_data,"%g\t%g\t%g\n",j,sqrt_calculator(j),sqrt(j));
	fprintf(stdout,"%1.4f\t%1.4f\t\t%1.4f\n",j,sqrt_calculator(j),sqrt(j));
}
return 0;
}








