#include<stdio.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<assert.h>

int sqrt_diff(double t, const double y[], double dydt[], void *params)
{
	dydt[0]=y[0]/(2*t);
	return GSL_SUCCESS;
}

double sqrt_calculator(double t) 
{
double t_ranged;
if(t==0.0){return 0.0;}
else if(t<1.0){t_ranged=1.0/t;}
else     {t_ranged=t;}

gsl_odeiv2_system sqrt_sys;
sqrt_sys.function = sqrt_diff;
sqrt_sys.jacobian = NULL; 
sqrt_sys.dimension = 1; 
sqrt_sys.params = NULL; 

double hstart=t*1e-6,epsabs=1e-6,epsrel=1e-6; 
const gsl_odeiv2_step_type *sqrt_stepper = gsl_odeiv2_step_rkf45; 
gsl_odeiv2_driver *sqrt_driver = gsl_odeiv2_driver_alloc_y_new(&sqrt_sys,sqrt_stepper,hstart,epsabs,epsrel);

double t_start=1.0, y[1]={1.0}; 
	      int status = gsl_odeiv2_driver_apply (sqrt_driver, &t_start, t_ranged, y);
	      if (status != GSL_SUCCESS)
		{
			printf ("ODE error, return value=%d\n", status);
	  	}
gsl_odeiv2_driver_free(sqrt_driver);

if(t<1.0){y[0]=1.0/y[0];}
return y[0];
}











