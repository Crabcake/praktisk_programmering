#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include<gsl/gsl_eigen.h>
#include<math.h>
#define M_PI 3.14159265358979323846
int main()
{
	
printf("\nEigenvalue Problem 1\n");

// Ex1

gsl_vector *b, *x;
gsl_matrix *A, *ACopy;  //Ax=b problem, ACopy for householder



b = gsl_vector_alloc(3);
gsl_vector_set(b,0,6.23);
gsl_vector_set(b,1,5.37);
gsl_vector_set(b,2,2.29);


A = gsl_matrix_alloc(3,3);
gsl_matrix_set(A,0,0,6.13);
gsl_matrix_set(A,0,1,-2.90);
gsl_matrix_set(A,0,2,5.86);

gsl_matrix_set(A,1,0,8.08);
gsl_matrix_set(A,1,1,-6.31);
gsl_matrix_set(A,1,2,-3.89);

gsl_matrix_set(A,2,0,-4.36);
gsl_matrix_set(A,2,1,1.00);
gsl_matrix_set(A,2,2,0.19);

x = gsl_vector_alloc(3);

// Making a copy of A because the householder destroys A...
ACopy = gsl_matrix_alloc(3,3);
gsl_matrix_memcpy(ACopy,A);

//Solving stuff according to manual
gsl_linalg_HH_solve(ACopy,b,x);
printf("\nThis is the solution for x:\n");
gsl_vector_fprintf(stdout,x,"%f");


//Attempt at multiplying a matrix and a vector...
gsl_vector *y; //0 vector for BLAS purposes
y = gsl_vector_calloc(3);
gsl_blas_dgemv(CblasNoTrans,1.0, A, x,0.0,y);
printf("\nBy multiplication of Ax we get the original b:\n");
gsl_vector_fprintf(stdout,y,"%f");


//freeing stuff
gsl_vector_free(b);
gsl_vector_free(x);
gsl_matrix_free(A);
gsl_vector_free(y);
gsl_matrix_free(ACopy);

//Now for the second part of the problem:
printf("\n\nProblem 2\n");

int n = 200;
gsl_matrix *Hmark;
Hmark = gsl_matrix_calloc(n,n); //Making a zero matrix
//Setting the diagonal and both neighboughs
for(int i=0;i<n-1;i++){
gsl_matrix_set(Hmark,i,i,-2);
gsl_matrix_set(Hmark,i+1,i,1);
gsl_matrix_set(Hmark,i,i+1,1);
}
gsl_matrix_set(Hmark,n-1,n-1,-2);

//scaling factor
double s=1.0/((double)n+1.0);
gsl_matrix_scale(Hmark,-1.0/s/s);

//gsl_eigen_symmv computes eigenvalues and eigenvectors for real symmetric matrix A, the w matrix is for extra workspace for the function. A is changed/destroyed. eigenvalues are stored in eval, eigenvectors in evec
gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n);
gsl_vector *eval;
gsl_matrix *evec;
eval = gsl_vector_alloc(n);
evec = gsl_matrix_calloc(n,n);

//Calculating and sorting by ascending values
gsl_eigen_symmv(Hmark,eval,evec,w);
gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC);
//Checking some of the eigenvalues...
fprintf(stdout, "i	exact	calculated\n");
for (int i=0; i<5; i++)
{
double correct = M_PI*M_PI*(i+1)*(i+1);
double calculated = gsl_vector_get(eval,i);
fprintf(stdout,"%i	%g	%g\n",i,correct,calculated);
}
// Printing the first 3 eigenvectors from 0-1.
FILE *MYSTREAM = fopen("data.txt","w");

fprintf(MYSTREAM,"%g %g %g %g\n",0.0,0.0,0.0,0.0); //Zero values at start
	for (int k=0;k<n;k++){
	fprintf(MYSTREAM,"%g %g %g %g\n",((double)k+1.0)/((double)n+1.0),gsl_matrix_get(evec,k,0),gsl_matrix_get(evec,k,1),gsl_matrix_get(evec,k,2));
}
	fprintf(MYSTREAM,"%g %g %g %g\n",1.0,0.0,0.0,0.0); //last values		
	fprintf(MYSTREAM,"\n\n"); //Space between

printf("\nThe eigenvectors have been placed in a data file, data.txt\n");


//Comparing with cosine functions.
//Since the sign of the calculated values can change, we need to find this.
double n_new=(((double)n+1.0)/2.0);
FILE *SINEDATA = fopen("sin.txt","w");
fprintf(SINEDATA,"%g %g %g %g\n",0.0,0.0,0.0,0.0); //Zero values at start
int highressine = 500;
double sign0=(gsl_matrix_get(evec,1,0) > 0) ? 1 : ((gsl_matrix_get(evec,1,0) < 0) ? -1 : 0); //Finding the sign
double sign1=(gsl_matrix_get(evec,1,1) > 0) ? 1 : ((gsl_matrix_get(evec,1,1) < 0) ? -1 : 0);
double sign2=(gsl_matrix_get(evec,1,2) > 0) ? 1 : ((gsl_matrix_get(evec,1,2) < 0) ? -1 : 0);
        for (int k=0;k<highressine;k++){
	double x = ((double)k+1.0)/((double)highressine+1.0);
        fprintf(SINEDATA,"%g %g %g %g\n",x,sign0*sin(1.0*M_PI*x)/(sqrt(n_new)),sign1*sin(2.0*M_PI*x)/(sqrt(n_new)),sign2*sin(3.0*M_PI*x)/(sqrt(n_new)));
}
        fprintf(SINEDATA,"%g %g %g %g\n",1.0,0.0,0.0,0.0); //last values                
        fprintf(SINEDATA,"\n\n"); //Space between

printf("\nThe analytical sine functions are plotted and also placed in sin.txt\n");


//Free again
gsl_matrix_free(Hmark);
gsl_matrix_free(evec);
gsl_vector_free(eval);
gsl_eigen_symmv_free(w);

return 0;
}
