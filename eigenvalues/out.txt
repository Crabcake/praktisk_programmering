
Eigenvalue Problem 1

This is the solution for x:
-1.137902
-2.833031
0.851459

By multiplication of Ax we get the original b:
6.230000
5.370000
2.290000


Problem 2
i	exact	calculated
0	9.8696	9.8694
1	39.4784	39.4752
2	88.8264	88.8102
3	157.914	157.862
4	246.74	246.615

The eigenvectors have been placed in a data file, data.txt

The analytical sine functions are plotted and also placed in sin.txt
