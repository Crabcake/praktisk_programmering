#include<limits.h>
#include<float.h>
#include<stdio.h>
#include<math.h>
int equal(double a, double b, double tau, double epsilon);


void ex4(){

}

int main()
{

ex4();
printf("\nProblem 1:\nCalculating the maximum rep. integer using different loops and comparing it with INT_MAX variable\n");

int i_while=2100000000;
while(i_while+1>i_while)
{i_while++;}

int i_for;
for(i_for=i_while-100000; (i_for+1) > i_for ; i_for++)
{;}

int i_do_while=i_while-100000;
do {i_do_while++;}while(i_do_while+1 > i_do_while);

printf("This is the largest value of an integer type:\nUsing while: %i\nUsing for: %i\nUsing do-while: %i\nINT_MAX: %i\n",i_while,i_for,i_do_while,INT_MAX);

printf("\nNow the same for the lowest integer\n");

int i_while_low=-2100000000;
while(i_while_low-1<i_while_low)
{i_while_low--;}

int i_for_low;
for(i_for_low=i_while_low+100000; (i_for_low-1) < i_for_low ; i_for_low--)
{;}

int i_do_while_low=i_while_low+100000;
do {i_do_while_low--;}while(i_do_while_low-1 < i_do_while_low);

printf("This is the lowest value of an integer type:\nUsing while: %i\nUsing for: %i\nUsing do-while: %i\nINT_MIN: %i\n",i_while_low,i_for_low,i_do_while_low,INT_MIN);


printf("\nCalculating the machine epsilon for various data types using a while-loop;\n");
// 1iii

float f1=1;
while(1 != 1+f1)
{f1/=2;}
f1*=2;

double d1=1;
while(1 != 1+d1)
{d1/=2;}
d1*=2;

long double ld1=1.0L;
while(1 != 1+ld1)
{ld1/=2;}
ld1*=2;

printf("Calculated values:\nEpsilon float: %lg\nEpsilon double: %lg\nEpsilon ldouble: %Lg\n",f1,d1,ld1);
printf("Comparison values:\nFLT_EPSILON: %lg\nDBL_EPSILON: %lg\nLDBL_EPSILON: %Lg\n",FLT_EPSILON,DBL_EPSILON,LDBL_EPSILON);


printf("\nNow the same for using a do-while loop\n");
float f2=1;
do{f2/=2;}while(1 != 1+f2);
f2*=2;

double d2=1;
do{d2/=2;}while(1 != 1+d2);
d2*=2;


long double ld2=1.0L;
do{ld2/=2;}while(1 != 1+ld2);
ld2*=2;

printf("Epsilon float: %lg\nEpsilon double: %lg\nEpsilon ldouble: %Lg\n",f2,d2,ld2);

printf("\nAnd using a for-loop:\n"); 


float f3;
for(f3=1;1+f3!=1;f3/=2){} f3*=2;

double d3;
for(d3=1;1+d3!=1;d3/=2){} d3*=2;

long double ld3;
for(ld3=1;1+ld3!=1;ld3/=2){} ld3*=2;


printf("Epsilon float: %lg\nEpsilon double: %lg\nEpsilon ldouble: %Lg\n",f3,d3,ld3);


printf("\nProblem 2:\nExcercise looking at the difference between floats and doubles in terms of rounding errors/addition of small numbers to large numbers\n");
//2

{
int int_max=INT_MAX/3;
float sum_down_float=0;
for(int i=0;i<int_max;i++)
{sum_down_float+=1.0f/(int_max-i);}
printf("sum_down_float %.10lg\n",sum_down_float);

float sum_up_float=0;
for(int i=1;i<int_max;i++)
{sum_up_float+=1.0f/(i);}
printf("sum_up_float %.10g\n",sum_up_float);
}
printf("Explain difference: Small numbers can be added when starting from low numbers. So we reach higher numbers\n");
printf("1/x doesn't converge\n");
{
int int_max=INT_MAX/3;
double sum_down_double=0;
for(int i=0;i<int_max;i++)
{sum_down_double+=1.0/(int_max-i);}
printf("sum_down_double %.10g\n",sum_down_double);

double sum_up_double=0;
for(int i=1;i<int_max;i++)
{sum_up_double+=1.0/(i);}
printf("sum_up_double %.10g\n",sum_up_double);
}
printf("Now the precision has been increased allowing less roundoff errors.\n");
printf("\nThe following number is a result of comparing two doubles for equality to absolute precision epsilon and tau. These varaibles are defined in the main.c file. The following should result in 0, because 1.0 and 2.0 aren't the same to eps=0.1 and tau=0.1 ...:\n");
int ex3_return = equal(1.,2.,.1,.1);
printf("Equal?: %i\n",ex3_return);

return 0;
}
