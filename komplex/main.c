#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("\nTesting of functions:\nTesting komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);

	printf("\nTesting komplex new\n");
	komplex a_new=komplex_new(1.2, 1.4);
	komplex_print("The new complex number should be {1.2,1.4}\nIt is = ",a_new);

	printf("\nTesting komplex set\nWe will now change the value of the above complex value...\n");
	komplex_set(&a_new,1.4,1.2);
	komplex_print("It should now have the numbers switched: ",a_new);

	printf("\nTesting komplex_sub\n");
	komplex subtraction = komplex_sub(a_new,a_new);
	komplex_print("Subtracting the identical numbers from each other with the result: ",subtraction);

	
	komplex conjugate = komplex_conjugate(a_new);
	komplex_print("\nTesting the complex conjugate function on the number {1.4,1.2}, with result: ",conjugate);



	double absolute = komplex_abs(a_new);
	printf("\nNow testing the absolute value of the original number ({1.4,1.2}) number. The result is: %g\n",absolute);

	
	komplex exponential = komplex_exp(a_new);
	komplex_print("\nFinding the value of exp(number), with the previous number as the argument. The result is: ",exponential);
}
