#include<stdio.h>
#include"komplex.h"
#include<math.h>
#include<float.h>

#ifndef TAU
#define TAU 1e-6
#endif
#ifndef EPS
#define EPS 1e-6
#endif

const komplex komplex_I = { 0, 1 };



void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re , a.im - b.im};
	return result;
}


// Optionals

	
komplex komplex_conjugate(komplex a) {
	komplex z = {a.re,-a.im};
	return z;
}	

double komplex_abs(komplex a) {
	double b = pow(a.re,2);
	double c = pow(a.im,2);
	double z = sqrt(b+c);
	return z;
}

komplex komplex_exp(komplex a) {
	komplex	z;
	z.re = cos(a.im) * exp(a.re);
	z.im = sin(a.im) * exp(a.re);
	return z;
}

