#ifndef HAVE_KOMPLEX_H

struct komplex {double re; double im;};
typedef struct komplex komplex;

void 	komplex_print 	 (char* s, komplex z); //Print s and complex z  
void    komplex_set      (komplex* z, double x, double y); //z is set to complex   
komplex komplex_new      (double x, double y); //return a complex number  
komplex komplex_add      (komplex a, komplex b); //return addition of 2 complex numbers
komplex komplex_sub      (komplex a, komplex b); //subtraction of complex nr's


komplex komplex_conjugate(komplex z);            //complex conjugate
double komplex_abs      (komplex z);		 //absolute value
komplex komplex_exp      (komplex z);		 //exp of complex


#define HAVE_KOMPLEX_H
#endif


