#include<stdio.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
//#define M_PI 3.14159265358979323846
 

double rosenbrock(const gsl_vector *v, void *params) //Function to be minimized in problem 1. Following the GSL example function structure
{
	double x,y;
	x=gsl_vector_get(v,0);
	y=gsl_vector_get(v,1);
	double f = pow((1.0-x),2)+pow((100.0*(y-x*x)),2);
	return f;
}

//Problem 2 functions
struct exp_data{int n; double *time,*activity,*error;}; //Struct for containing experimental data for use in the function which needs to be minimized

double least_squares(const gsl_vector *v, void *params)
{
	double A0 = gsl_vector_get(v,0);
	double T  = gsl_vector_get(v,1);
	double B  = gsl_vector_get(v,2); //initial activity, lifetime, background

	struct exp_data *p = (struct exp_data*) params; //params inserted in struct
	int n = p->n;
	double *time = p->time;
	double *activity = p->activity;
	double *error = p->error;
	double sum=0;
	#define f(time) A0*exp(-(time)/T)+B //Makes the next formula much more compact
	for (int i =0;i<n;i++) //running through each data point
	{
	sum+= pow((f(time[i])-activity[i])/error[i],2);
	}
	return sum;
}



int main()
{
//Starting the first problem. Based on the method used in the GSL example.
{
printf("\nProblem 1\n");
size_t iter=0; //unsigned integ. Can hold any array index 
int status;

gsl_multimin_function rosen_func;
rosen_func.n=2;//x,y
rosen_func.f=rosenbrock;

//Initialize and set starting values
gsl_vector *v;
v = gsl_vector_alloc(rosen_func.n);
gsl_vector_set(v,0, 5);
gsl_vector_set(v,1, 5);

//Stepsize needs to be a gsl vector like the above
gsl_vector *step;
step = gsl_vector_alloc(rosen_func.n);
gsl_vector_set(step,0,1.0);
gsl_vector_set(step,1,1.0);

//Setting workspace
gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,rosen_func.n);
gsl_multimin_fminimizer_set(s,&rosen_func,v,step); 
printf("iter\tx\ty\tf_val\n");
do
	{
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);
		if(status)
			{break;}
		//testing for success:
		double size = gsl_multimin_fminimizer_size(s);
		status = gsl_multimin_test_size(size,1e-4); //last value is acceptance for convergance.
		if ((iter-1)%10==0)
				{printf("%lu\t%.4f\t%.4f\t%.4f\n",iter,
				gsl_vector_get(gsl_multimin_fminimizer_x(s),0),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),1),
				gsl_multimin_fminimizer_minimum(s));} 
		if (status==GSL_SUCCESS)
			{printf("Minimum found\niter\tx\ty\tf_val\n");
			 printf("%lu\t%.4f\t%.4f\t%.4f\n",iter,
				gsl_vector_get(gsl_multimin_fminimizer_x(s),0),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),1),
				gsl_multimin_fminimizer_minimum(s));}
		
	}
while (status == GSL_CONTINUE && iter < 1000); //max of 100 interations

gsl_multimin_fminimizer_free(s);
gsl_vector_free(v);
gsl_vector_free(step);
}



//Starting the second problem
{
printf("\nProblem 2\n");

size_t iter=0; //unsigned integ. Can hold any array index 
int status;

gsl_multimin_function least_squares_func;
least_squares_func.n=3;//A0,T,B
least_squares_func.f=least_squares;

//params:
double time[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double activity[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double error[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(time)/sizeof(time[0]);

struct exp_data params;
params.n=n;
params.time=time;
params.activity=activity;
params.error=error;
least_squares_func.params=(void*)&params;

//Initialize and set starting values
gsl_vector *v;
v = gsl_vector_alloc(least_squares_func.n);
gsl_vector_set(v,0, 10.0);
gsl_vector_set(v,1, 10.0);
gsl_vector_set(v,2, 10.0);

//Stepsize needs to be a gsl vector like the above
gsl_vector *step;
step = gsl_vector_alloc(least_squares_func.n);
gsl_vector_set(step,0,2.0);
gsl_vector_set(step,1,2.0);
gsl_vector_set(step,2,2.0);

//Setting workspace
gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,least_squares_func.n);
gsl_multimin_fminimizer_set(s,&least_squares_func,v,step); 
printf("iter\tA0\tLtime\tNoise\tf_val\n");
do
	{
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);
		if(status)
			{break;}
		//testing for success:
		double size = gsl_multimin_fminimizer_size(s);
		status = gsl_multimin_test_size(size,1e-3); //last value is acceptance for convergance.
		if ((iter-1)%10==0)
				{printf("%lu\t%2.4f\t%2.4f\t%2.4f\t%2.4f\n",iter,
				gsl_vector_get(gsl_multimin_fminimizer_x(s),0),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),1),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),2),
				gsl_multimin_fminimizer_minimum(s));} //_x get current value, _minimum gets the value of the function at that point.
	
		if (status==GSL_SUCCESS)
			{printf("Minimum found\niter\tA0\tLtime\tNoise\tfval\n");
			 printf("%lu\t%2.4f\t%2.4f\t%2.4f\t%2.4f\n",iter,
				gsl_vector_get(gsl_multimin_fminimizer_x(s),0),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),1),
				gsl_vector_get(gsl_multimin_fminimizer_x(s),2),
				gsl_multimin_fminimizer_minimum(s));} //_x get current value, _minimum gets the value of the function at that point.
	}
while (status == GSL_CONTINUE && iter < 1000); //max of 100 interations

//making data for plot:
double t_min=0.47,t_max=8.95,reso=100.0;
double del_t=(t_max-t_min)/reso;
FILE *FITDATA = fopen("fitdata.txt","w");
for (double i=t_min;i<(t_max+del_t);i+=del_t)
{	
	double Act_t = gsl_vector_get(gsl_multimin_fminimizer_x(s),0)*exp(-i/gsl_vector_get(gsl_multimin_fminimizer_x(s),1))+gsl_vector_get(gsl_multimin_fminimizer_x(s),2);
	fprintf(FITDATA,"%2.4g\t%2.4g\n",i,Act_t);
}

//making file with original data:
FILE *EXPDATA = fopen("expdata.txt","w");
for (int j=0;j<n;j++)
{
fprintf(EXPDATA,"%2.4g\t%2.4g\n",time[j],activity[j]);
}

gsl_multimin_fminimizer_free(s);
gsl_vector_free(v);
gsl_vector_free(step);
}
	
return 0;
}
