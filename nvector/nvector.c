#include<stdio.h>
#include"nvector.h"
#include<math.h>
#include<assert.h>
#include<stdlib.h>


nvector* nvector_alloc(int n)
{
	nvector* v = (nvector*) malloc(sizeof(nvector));
	(*v).size  = n;
	(*v).data  = (double*) malloc(n*sizeof(double));
	if (v == NULL)
		fprintf(stderr,"error in nvector_alloc\n.");
	return v;
}

void nvector_free(nvector **v)  //Changed compared to Dimi's version. This makes it possible to check if the pointer to nvector has been cleared. Not super useful, but it makes kinda makes it possible to check if the function finished. Of course errors would pop up otherwise....
{
	free((**v).data);
	free(*v);
	*v = NULL;
}

void nvector_set(nvector * v, int i, double value) //Check if the changed entry is within array...
{
	assert(0 <= i && i < (*v).size);
	(*v).data[i] = value;
}

double nvector_get(nvector * v, int i) //Trivial
{
	assert(0 <= i && i < (*v).size);
	return (*v).data[i];
}


void nvector_print(char *s, nvector *v) 
{
printf("%s\n", s);
	for(int i = 0; i < (*v).size ; i++)
	printf("%g ",(*v).data[i]);
	printf("\n");
}


double nvector_dot_product(nvector *v, nvector *u)
{
	assert((*v).size == (*u).size);
	double SUM;
	for (int i = 0; i < (*v).size; i++)
	SUM = SUM + (*v).data[i]*(*u).data[i];
	return SUM;
}



void nvector_add(nvector *a, nvector *b)
{
	assert((*a).size == (*b).size);
	for (int i = 0; i < (*a).size; i++) {
		double s = nvector_get(a, i) + nvector_get(b, i);
		nvector_set(a, i, s);
	}
}

int double_equal(double a, double b)
{
	double TAU = 1e-6, EPS = 1e-6;
	if (fabs(a - b) < TAU)
		return 1;
	if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
		return 1;
	return 0;
}

int nvector_equal(nvector * a, nvector * b)
{
	assert((*a).size == (*b).size);
	for (int i = 0; i < (*a).size; i++)
		if (!double_equal((*a).data[i], (*b).data[i])) //Logical negation, so returns zero when different
			return 0;
	return 1;
}




