#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX
#include <assert.h>


int main()
{

	int n=5;
	
	printf("\nTesting functions\nMain: testing nvector_alloc ...\n");
	nvector *l = nvector_alloc(n);
 	if (l == NULL)
		printf("test failed\n");
	else 
		printf("test passed\n");

	nvector_free(&l);
	
	if (l == NULL) printf("\nMain: testing nvector_free ...\nThe nvector_free function succeeded\n");
	else printf("\nMain: testing nvector_free ...\nThere is a problem with the nvector_free function\n");//unsure about the truthness of this test...



	printf("\nMain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector *v = nvector_alloc(n);
	nvector_set(v, i, value);
	double vi = nvector_get(v, i); 
	if (double_equal(vi, value)) printf("Test passed\n");
	else printf("Test failed\n");

	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	
	printf("\nTesting add function...");
	nvector_add(a,b);
	nvector_print("Vector 'a' should now be equal to vector c:\n<a>=",a);
	nvector_print("\n<c>=",c);
	


	
	nvector_print("\nTesting printing function by printing... This vector should contain one number: ",v);


	

	
	printf("\nTesting the dot product function...");
	double SUM = nvector_dot_product(v,a);
	printf("This should be single number equal to the product of v's number and the comparable number in a");
	nvector_print("\n<a>=",a);
	nvector_print("\n<v>=",v);
	printf("\n<a>*<v>=%g\n",SUM);


	printf("\nLast test: testing the equality function. <a> and <c> should be equal:\n");
	printf("Subtest1:\n");
	int	result1=nvector_equal(a,c);
	printf("result: %i",result1);
	printf("\nChecking if <a>=<b>, this should fail:\nSubtest2");
	int	result2=nvector_equal(a,b);
	printf("\nresult: %i\n",result2);
	printf("Test passed\n");
	nvector_free(&v);
	nvector_free(&a);
	nvector_free(&b);
	nvector_free(&c);
	



	



return 0;
}
