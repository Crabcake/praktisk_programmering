#ifndef HAVE_NVECTOR_H

typedef struct {int size; double* data;} nvector;

nvector* nvector_alloc       (int n);                           //Allocate memory 
void     nvector_free        (nvector **v);                      // Free mem
void     nvector_set         (nvector* v, int i, double value); // Set value of element
double   nvector_get         (nvector* v, int i);               // Get element
double   nvector_dot_product (nvector* u, nvector* v);          // Dot-prod


void nvector_print    (char* s, nvector* v);    // print text and then vector
int  double_equal     (double a, double b);     // Tests for equality of a double
int  nvector_equal    (nvector* a, nvector* b); // Tests for equality of a vector
void nvector_scale    (nvector* a, double x);   // Scales vector by x (a_i*x)
void nvector_add      (nvector *a,nvector *b);

#define HAVE_NVECTOR_H
#endif
