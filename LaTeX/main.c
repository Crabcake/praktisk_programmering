#include<stdio.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<gsl/gsl_matrix.h>

int err_diff(double t, const double y[], double dydt[], void *params) //Simply following the gsl recipe so far. This will contain the diff eq:
{
	dydt[0]=2*pow(M_PI,-0.5)*exp(-t*t); 
	return GSL_SUCCESS;
}

double Auxillary(double t) 
{
//The diff eq system
gsl_odeiv2_system diff_sys;
diff_sys.function = err_diff; //Choosing which diff eq system to solve
diff_sys.jacobian = NULL; //No need for jacobian so it is replaced by NULL as described in defining the ODE system
diff_sys.dimension = 1; //2 dim system...
diff_sys.params = NULL; //epsilon/energy

//Now for the driver. It needs certain values
double hstart=copysign(1e-6,t),epsabs=1e-6,epsrel=1e-6; //initial stepsize(and direction depending on t/the value we want),epsabs and epsrel
const gsl_odeiv2_step_type *diff_stepper = gsl_odeiv2_step_rkf45; //Defining step function. This is good for general ode problems.
gsl_odeiv2_driver *diff_driver = gsl_odeiv2_driver_alloc_y_new(&diff_sys,diff_stepper,hstart,epsabs,epsrel); //See GSL ODE DRIVER

//Following GSL example applying solver
double t_start=0.0, y[1]={0.0}; //the y contains the start value
	      int status = gsl_odeiv2_driver_apply (diff_driver, &t_start, t, y);
	      if (status != GSL_SUCCESS)
		{
			printf ("ODE error, return value=%d\n", status);
			//break;
	  	}
gsl_odeiv2_driver_free(diff_driver);
return y[0];
}

int main(int argc, char *argv[])
{
if(argc!=4){
	printf("You need to supply a, b and dx. With b>a and preferably a nice dx...\nExiting\n");
	exit(0);}
		       double a,b,dx;
		       sscanf(argv[1], "%lf", &a);
 		       sscanf(argv[2], "%lf", &b);
  		       sscanf(argv[3], "%lf", &dx);
printf("\n\nLaTeX Problem\n\nx\terf(x)\n");
FILE *diff_eq_data = fopen("data.txt","w");
for (double j=a;j<(b+dx*0.001);j+=dx)
{
	fprintf(diff_eq_data,"%g\t%g\n",j,Auxillary(j));
	fprintf(stdout,"%1.4f\t%1.4f\n",j,Auxillary(j));
}
	printf("\nSee plot.pdf for graph\nSee the latex pdf for latex example...\n");	


return 0;
}








