#include <gsl/gsl_errno.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>


//For problem 1
int rosenbrock_gradient (const gsl_vector *x, void *params, gsl_vector *f) 
{
	//Defining x0=x,x1=y,f_x=f0,f_y=f1
	double x0,x1,f0,f1;
	
	x0 = gsl_vector_get(x,0);
	x1 = gsl_vector_get(x,1);
	
	f0 = -2.0*(x0 - 1.0 + 200*(x0*x0*x0-x1*x0));
	f1 = 200*(x1 - x0*x0);

	gsl_vector_set(f,0,f0);
	gsl_vector_set(f,1,f1);

	return GSL_SUCCESS;
}


//For problem 2
double Me_auxillary(double e, double r);

int master(const gsl_vector *x, void *params, gsl_vector *f) //Now the problem looks like problem 1, if we ignore the behind lying diff equation...
{
	double energy,f_inf;
	const double r_max = 10.0;
	
	energy = gsl_vector_get(x,0);

	f_inf = Me_auxillary(energy,r_max);
	
	gsl_vector_set(f,0,f_inf);

	return GSL_SUCCESS;
}


int main() 
{

{
printf("\nStarting Problem 1\n"); //Following the general structure of the GSL example

//Very similar to the previous exercise
const gsl_multiroot_fsolver_type *T;
gsl_multiroot_fsolver *s;

int status;
size_t iter=0;

const size_t n = 2;
gsl_multiroot_function rosen_grad = {&rosenbrock_gradient, n, NULL}; //No parameters needed

//Setting start x,y
double x_start[2] = {1.5, 1.5};

gsl_vector *x = gsl_vector_alloc(n);
gsl_vector_set(x,0,x_start[0]); //x
gsl_vector_set(x,1,x_start[1]); //y

// Setting the solver
T = gsl_multiroot_fsolver_hybrids;
s = gsl_multiroot_fsolver_alloc(T,2);
gsl_multiroot_fsolver_set(s,&rosen_grad,x);

//iterating
do
{
	iter++;
	status = gsl_multiroot_fsolver_iterate(s);
	if(status){break;}
	if((iter-1)%10==0){printf("iteration=%lu\tx=%g,\ty=%g,\tfval=%g\n",iter,gsl_vector_get(s->x,0),
			gsl_vector_get(s->x,1),gsl_vector_get(s->f,0));} //current values are in s->f/x for f and x respectively. (f is a sum over the residuals)
	status = gsl_multiroot_test_residual(s->f,1e-6); //cheking if acceptable
}	
while(status==GSL_CONTINUE && iter < 1000);

printf("Root found\niteration=%lu\tx=%g,\ty=%g,\tfval=%g\n",iter,gsl_vector_get(s->x,0),
			gsl_vector_get(s->x,1),gsl_vector_get(s->f,0));

gsl_vector_free(x);
gsl_multiroot_fsolver_free(s);
}



{
printf("\n\n Starting Problem 2\n"); // The ODE solving will happen in a another file. See top.

const gsl_multiroot_fsolver_type *T;
gsl_multiroot_fsolver *s;

int status;
size_t iter=0;

const size_t n = 1;
gsl_multiroot_function hydrogen_function = {&master, n, NULL}; //No parameters needed

double x_start[1] = {-10.5};
gsl_vector *x = gsl_vector_alloc(n);
gsl_vector_set(x,0,x_start[0]); 

// Setting the solver
T = gsl_multiroot_fsolver_hybrids;
s = gsl_multiroot_fsolver_alloc(T,1);
gsl_multiroot_fsolver_set(s,&hydrogen_function,x);

//iterating
do
{
	iter++;
	status = gsl_multiroot_fsolver_iterate(s);
	if(status){break;}
	if((iter-1)%10==0){printf("iteration=%lu\tenergy=%g,\tfval=%g\n",iter,gsl_vector_get(s->x,0),gsl_vector_get(s->f,0));} //current values are in s->f/x for f and x respectively. (f is a sum over the residuals)
	status = gsl_multiroot_test_residual(s->f,1e-7); //cheking if acceptable
}	
while(status==GSL_CONTINUE && iter < 1000);

printf("Root found\niteration=%lu\tenergy=%g,\tfval=%g\n",iter,gsl_vector_get(s->x,0),gsl_vector_get(s->f,0));

int j_points = 200;
double r_max = 10.0;
FILE *DATA = fopen("data.txt","w");


for (int j = 1; j<j_points; j++)
{
	double r = r_max*j/j_points;
	double fval=Me_auxillary(gsl_vector_get(s->x,0),r);
	fprintf(DATA,"%g\t%g\n",r,fval);
}


gsl_vector_free(x);
gsl_multiroot_fsolver_free(s);




}


return 0;
}
