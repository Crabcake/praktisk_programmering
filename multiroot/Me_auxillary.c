#include<stdio.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h> //Has gsl success and failure functions
#include<math.h>
#include<gsl/gsl_matrix.h>

int Hydrogen_diff(double t, const double y[], double dydt[], void *params) //Simply following the gsl recipe so far. This will contain the diff eq:
{
	double epsilon = *(double*) params;
	dydt[0]=y[1]; //coupled first order instead
	dydt[1]=-2.0*( epsilon*y[0] + (1.0/t)*y[0] ); //The diff equation described
	return GSL_SUCCESS;
}

double Me_auxillary(double epsilon, double t) //Think we need t to define the value we evolve to in the master function which is the function we want to root find on in the main.c. Technically this makes it a 1 dim problem in energy, no?
{

double r_min = 1e-3; //The point where the r->0 limit is enforced

//The diff eq system
gsl_odeiv2_system hydrogen_sys;
hydrogen_sys.function = Hydrogen_diff; //Choosing which diff eq system to solve
hydrogen_sys.jacobian = NULL; //No need for jacobian so it is replaced by NULL as described in defining the ODE system
hydrogen_sys.dimension = 2; //2 dim system...
hydrogen_sys.params = (void*)&epsilon; //epsilon/energy

//Now for the driver. It needs certain values
double hstart=1e-6,epsabs=1e-6,epsrel=1e-6; //initial stepsize,epsabs and epsrel
const gsl_odeiv2_step_type *hydrogen_stepper = gsl_odeiv2_step_rkf45; //Defining step function. This is good for general ode problems.
gsl_odeiv2_driver *hydrogen_driver = gsl_odeiv2_driver_alloc_y_new(&hydrogen_sys,hydrogen_stepper,hstart,epsabs,epsrel); //See GSL ODE DRIVER

//Following GSL example applying solver
double t_start=r_min, y[2]={t_start-t_start*t_start, 1-2.0*t_start}; //the y contains the start value and its derivative as described in the exercise as the r->0 limit.
	      int status = gsl_odeiv2_driver_apply (hydrogen_driver, &t_start, t, y);
	      if (status != GSL_SUCCESS)
		{
			printf ("ODE error, return value=%d\n", status);
			//break;
	  	}
gsl_odeiv2_driver_free(hydrogen_driver);
return y[0];
}
